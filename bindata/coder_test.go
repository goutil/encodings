/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package bindata

import "testing"

func eqTest(t *testing.T, p, b, r interface{}) {
	if p != r {
		t.Errorf("%v -> %v -> %v", p, b, r)
	} else {
		t.Logf("%v -> %v -> %v", p, b, r)
	}
}

func TestBool(t *testing.T) {
	P := true
	B := memdump(P)
	R, _ := Bool(B)
	eqTest(t, P, B, R)
}

func TestInt(t *testing.T) {
	var P int = 10
	B := memdump(P)
	R, _ := Int(B)
	eqTest(t, P, B, R)
}

func TestInt8(t *testing.T) {
	var P int8 = 10
	B := memdump(P)
	R, _ := Int8(B)
	eqTest(t, P, B, R)
}

func TestInt16(t *testing.T) {
	var P int16 = 10
	B := memdump(P)
	R, _ := Int16(B)
	eqTest(t, P, B, R)
}

func TestInt32(t *testing.T) {
	var P int32 = 10
	B := memdump(P)
	R, _ := Int32(B)
	eqTest(t, P, B, R)
}

func TestInt64(t *testing.T) {
	var P int64 = 10
	B := memdump(P)
	R, _ := Int64(B)
	eqTest(t, P, B, R)
}

func TestUint(t *testing.T) {
	var P uint = 10
	B := memdump(P)
	R, _ := Uint(B)
	eqTest(t, P, B, R)
}

func TestUint8(t *testing.T) {
	var P uint8 = 10
	B := memdump(P)
	R, _ := Uint8(B)
	eqTest(t, P, B, R)
}

func TestUint16(t *testing.T) {
	var P uint16 = 10
	B := memdump(P)
	R, _ := Uint16(B)
	eqTest(t, P, B, R)
}

func TestUint32(t *testing.T) {
	var P uint32 = 10
	B := memdump(P)
	R, _ := Uint32(B)
	eqTest(t, P, B, R)
}

func TestUint64(t *testing.T) {
	var P uint64 = 10
	B := memdump(P)
	R, _ := Uint64(B)
	eqTest(t, P, B, R)
}

func TestUintptr(t *testing.T) {
	var P uintptr = 10
	B := memdump(P)
	R, _ := Uintptr(B)
	eqTest(t, P, B, R)
}

func TestFloat32(t *testing.T) {
	var P float32 = 10.10
	B := memdump(P)
	R, _ := Float32(B)
	eqTest(t, P, B, R)
}

func TestFloat64(t *testing.T) {
	var P float64 = 10.10
	B := memdump(P)
	R, _ := Float64(B)
	eqTest(t, P, B, R)
}

func TestComplex64(t *testing.T) {
	var P complex64 = 10.10+10i
	B := memdump(P)
	R, _ := Complex64(B)
	eqTest(t, P, B, R)
}

func TestComplex128(t *testing.T) {
	var P complex128 = 10.10+10i
	B := memdump(P)
	R, _ := Complex128(B)
	eqTest(t, P, B, R)
}

func TestString(t *testing.T) {
	var P = "abcdf"
	B := memdump(P)
	R, _ := String(B)
	eqTest(t, P, B, R)
}